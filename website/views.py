from django.shortcuts import render
from django.core.mail import send_mail


def index(request):
    return render(request, 'website/index.html')

def articles(request):
    return render(request, 'website/articles.html')

def cv(request):
    return render(request, 'website/cv_page.html')

def about(request):
    return render(request, 'website/about.html')

def contact(request):
    context = {}
    if request.method == 'POST':
        try:
            send_mail(request.POST.get("contactSubject", "defaultSubject"),
                      request.POST.get("contactName", "defaultContactName") + '\n' +
                      request.POST.get("contactEmail", "defaultEmail@email.com") + '\n' +
                      request.POST.get("contactPhone", "00 00 00 00 00") + '\n' +
                      request.POST.get("contactMessage", "defaultMessage"),
                      request.POST.get("contactEmail", "defaultEmail@email.com"),
                      ['romainzanchi30+websitecontact@gmail.com'], fail_silently=False)
            context['sendSuccess'] = True
        except:
            context['sendError'] = True
    return render(request, 'website/contact.html', context)
