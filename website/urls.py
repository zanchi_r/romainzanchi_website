from django.conf.urls import patterns, include, url

from website import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^articles/', views.articles, name='articles'),
    url(r'^about/', views.about, name='about'),
    url(r'^cv/', views.cv, name='cv'),
    url(r'^contact/', views.contact, name='contact'),
)
