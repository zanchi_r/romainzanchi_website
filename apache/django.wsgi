import os
import sys
from django.core.wsgi import get_wsgi_application

path = '/home/zanchi_r/romainzanchi_website'
if path not in sys.path:
    sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'romainzanchi_website.settings'

import django.core.handlers.wsgi
application = get_wsgi_application()
